import json
import socket
from typing import Dict, Any

def pretty_output(response: Dict[str, any]):
    print(f"Status of operation: {response['status']}\n")
    if response.get("data", None) is None:
        return
    print("Operation data:")
    print(response["data"])


def helpInfo() -> None:
    print("Commands that you should use")
    print("\tconnect - allows you to connect for yours virtual machine via username and password")
    print("\tconnected - returns a list of connected clients")
    print("\tauthorized - return a list of all non-deleted clients")
    print("\tall - return a list of all clients (with deleted)")
    print("\tclose - allows you to close connection for your virtual machine and leave app")
    print("\tcreds - allows you to update credentials for your virtaul machine")
    print("\tdrives - return a list for all hard drives with it clients")
    print("\tdelete - allows you to delete a virtual machine")
    print("\tstats - returns a statistics of server")
    print("\tupdate - allows u to update virtual machine")


class Client:
    def __init__(self, host: str, port: int) -> None:
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.connect((host, port))
        self.handlers = {
            "connect": self.connect,
            "connected": self.get_connected,
            "authorized": self.get_authorized,
            "all": self.get_all,
            "close": self.close_connection,
            "creds": self.update_creds,
            "drives": self.get_drives,
            "delete": self.delete_client,
            "stats": self.get_stats,
            "update": self.update_client,
        }
        self.need_auth = ("creds", "delete", "update")
        self.connected_id = None

    def run(self) -> None:
        request = None
        try:
            while request not in ("close", "delete"):
                request = input(">>>")
                if request == "connect" and self.connected_id is not None:
                    print("You should quit before changing connected virtual machine")
                    continue
                if request in self.need_auth and self.connected_id is None:
                    print("You shoud authorize before use it")
                    continue
                
                payload = self.handlers.get(request, helpInfo)()
                
                if payload is None:
                    continue
                
                self.server.send(payload.encode("utf8"))
                response = self.server.recv(1024).decode("utf8")
                response = json.loads(response)
                
                if request == "connect" and response["status"] == "OK":
                    self.connected_id = response["data"]["id"]
                
                if request == "delete" and response["status"] == "OK":
                    self.connected_id = None
                
                pretty_output(response)

        except KeyboardInterrupt:
            req = self.close_connection()
            self.server.send(req.encode("utf8"))
            self.server.close()

    def connect(self):
        username = input("Input your username:")
        password = input("Input your password:")
        return json.dumps(
            {"action": "connect", "payload": {"username": username, "password": password}}
        )


    def get_connected(self):
        return json.dumps({"action": "connected", "payload": {}})


    def get_authorized(self):
        return json.dumps({"action": "authorized", "payload": {}})


    def get_all(self):
        return json.dumps({"action": "all", "payload": {}})


    def close_connection(self):
        return json.dumps({"action": "close", "payload": {"id": self.connected_id}})


    def update_creds(self):
        username = input("Input new username:")
        password = input("Input new password:")
        return json.dumps(
            {
                "action": "creds",
                "payload": {"id": self.connected_id, "username": username, "password": password},
            }
        )


    def get_drives(self):
        return json.dumps({"action": "drives", "payload": {}})


    def delete_client(self):
        return json.dumps({"action": "delete", "payload": {"id": self.connected_id}})


    def get_stats(self):
        return json.dumps({"action": "stats", "payload": {}})


    def update_client(self):
        ram_size = input("Input your ram_size:")
        cpu_count = input("Input your cpu count:")
        drive_size = input("Input your drive size:")

        if any(int(x) <= 0 for x in (ram_size, cpu_count, drive_size)):
            print("Ram_size/CpuCount/DriveSize can't be lower and eq 0")
            return None
        
        return json.dumps(
            {
                "action": "update",
                "payload": {
                    "id": self.connected_id,
                    "ram_size": ram_size,
                    "cpu_count": cpu_count,
                    "drive_size": drive_size,
                },
            }
        )
