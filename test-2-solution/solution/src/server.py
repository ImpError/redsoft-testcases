from typing import Dict

import aiosqlite
import asyncio
import socket
import json
import logging
import os

from dynaconf import Dynaconf

from src.handlers import (
    connect_client,
    get_connected,
    get_authorized,
    get_all,
    close_connection,
    update_credentials,
    get_all_hard_drives,
    delete_client,
    get_stats,
    update_client,
)


def setup_handlers() -> Dict:
    return {
        "connect": connect_client,
        "connected": get_connected,
        "authorized": get_authorized,
        "all": get_all,
        "close": close_connection,
        "creds": update_credentials,
        "drives": get_all_hard_drives,
        "delete": delete_client,
        "stats": get_stats,
        "update": update_client,
    }


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Server:
    def __init__(
        self,
        config: Dict[str, str | int],
        db_conn: aiosqlite.Connection,
        handlers: Dict,
        srv: socket.socket,
    ) -> None:
        self._config = config
        self._db_conn = db_conn
        self._handlers = handlers
        self._socket = srv

    @classmethod
    async def from_config(cls, settings_path: str) -> "Server":
        config = Dynaconf(
            envvar_prefix="DYNACONF",
            settings_files=[
                settings_path + "/settings.toml",
                settings_path + "/.secrets.toml",
            ],
        )

        logger.info("Creating connection...")

        db_path = os.path.join(os.getcwd(), config.db.db_path)
        connection = await aiosqlite.connect(db_path)

        handlers = setup_handlers()

        logger.info("Creating socket...")
        srv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        srv.bind((config.api.host, config.api.port))
        srv.listen(config.api.connections)
        srv.setblocking(False)

        server = Server(
            config=config,
            db_conn=connection,
            handlers=handlers,
            srv=srv,
        )

        return server

    async def handle_client(self, client):
        logging.info("Client connected")
        loop = asyncio.get_event_loop()
        try:
            while True:
                request = (
                    await loop.sock_recv(client, self._config.server.buffer_size)
                ).decode("utf8")
                req_payload = json.loads(request)

                response = await self._handlers[req_payload["action"]](
                    req_payload["payload"], self._db_conn
                )
                await loop.sock_sendall(client, response.encode("utf8"))
                if req_payload["action"] in ("close", "delete"):
                    break
        finally:
            client.close()

    async def listen_and_serve(self) -> None:
        logger.info("Server starting...")
        loop = asyncio.get_event_loop()

        while True:
            client, _ = await loop.sock_accept(self._socket)
            loop.create_task(self.handle_client(client))
            

    async def dispose(self) -> None:
        logger.info("Disposing server...")
        self._socket.close()
        try:
            await self._db_conn.close()
        except Exception as unexcpected_error:
            raise RuntimeWarning("Not closed sqlite connection")
        logger.info("Disposed successfully")
