from typing import Dict, Any
import logging
import json
from aiosqlite import Connection, OperationalError

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


async def connect_client(payload: Dict[str, Any], db_conn: Connection) -> str:
    stmt = "SELECT * FROM clients WHERE username=? AND password=? AND visible=TRUE"
    cursor = await db_conn.execute(stmt, (payload["username"], payload["password"]))
    data = await cursor.fetchone()
    if data is None:
        return json.dumps(
            {
                "status": "Failed",
                "data": {
                    "info": f"Username with {payload['username']} or password with {payload['password']} doesnt exists",
                },
            }
        )

    await cursor.close()
    await db_conn.execute(
        "UPDATE clients SET connected=TRUE WHERE username=? AND password=?",
        (payload["username"], payload["password"]),
    )
    await db_conn.commit()
    return json.dumps(
        {
            "status": "OK",
            "data": {
                "id": data[0],
                "username": data[1],
            },
        }
    )


async def get_connected(payload: Dict[str, Any], db_conn: Connection) -> str:
    stmt = f"""SELECT clients.*, hard_drives.drive_name, hard_drives.drive_size
                FROM clients 
                JOIN hard_drives 
                ON clients.id = hard_drives.client_id 
                WHERE connected=TRUE AND visible=TRUE;"""

    cursor = await db_conn.execute(stmt)
    rows = await cursor.fetchall()
    data = dict()
    for row in rows:
        if row[0] not in data:
            data[row[0]] = {
                "username": row[1],
                "ram_size": row[4],
                "cpu_count": row[5],
                "drives": [
                    {
                        "name": row[7],
                        "size": row[8],
                    }
                ],
            }
        else:
            data[row[0]]["drives"].append(
                {
                    "name": row[7],
                    "size": row[8],
                }
            )

    await cursor.close()
    return json.dumps({"status": "OK", "data": data})


async def get_authorized(payload: Dict[str, Any], db_conn: Connection) -> str:
    stmt = f"""SELECT clients.*, hard_drives.drive_name, hard_drives.drive_size
                FROM clients 
                JOIN hard_drives 
                ON clients.id = hard_drives.client_id 
                WHERE visible=TRUE;"""

    cursor = await db_conn.execute(stmt)
    rows = await cursor.fetchall()
    data = dict()
    for row in rows:
        if row[0] not in data:
            data[row[0]] = {
                "username": row[1],
                "ram_size": row[4],
                "cpu_count": row[5],
                "drives": [
                    {
                        "name": row[7],
                        "size": row[8],
                    }
                ],
            }
        else:
            data[row[0]]["drives"].append(
                {
                    "name": row[7],
                    "size": row[8],
                }
            )

    await cursor.close()
    return json.dumps({"status": "OK", "data": data})


async def get_all(payload: Dict[str, Any], db_conn: Connection) -> str:
    stmt = f"""SELECT clients.*, hard_drives.drive_name, hard_drives.drive_size
                FROM clients 
                JOIN hard_drives 
                ON clients.id = hard_drives.client_id;"""

    cursor = await db_conn.execute(stmt)
    rows = await cursor.fetchall()
    data = dict()
    for row in rows:
        if row[0] not in data:
            data[row[0]] = {
                "username": row[1],
                "ram_size": row[4],
                "cpu_count": row[5],
                "drives": [
                    {
                        "name": row[7],
                        "size": row[8],
                    }
                ],
            }
        else:
            data[row[0]]["drives"].append(
                {
                    "name": row[7],
                    "size": row[8],
                }
            )

    await cursor.close()
    return json.dumps({"status": "OK", "data": data})


async def close_connection(payload: Dict[str, Any], db_conn: Connection) -> str:
    if payload.get("id", None) is None:
        return json.dumps(
        {
            "status": "Closed without connection",
        }
    )
    await db_conn.execute(
        "UPDATE clients SET connected=FALSE WHERE id=?", (payload["id"], )
    )
    await db_conn.commit()
    return json.dumps(
        {
            "status": "OK",
        }
    )


async def update_credentials(payload: Dict[str, Any], db_conn: Connection) -> str:
    await db_conn.execute(
        "UPDATE clients SET username=?, password=? WHERE id=?",
        (payload["username"], payload["password"], payload["id"]),
    )
    await db_conn.commit()
    return json.dumps(
        {
            "status": "OK",
        }
    )


async def get_all_hard_drives(payload: Dict[str, Any], db_conn: Connection) -> str:
    stmt = f"""SELECT * 
                FROM hard_drives
                JOIN clients
                ON hard_drives.client_id=clients.id;
            """

    cursor = await db_conn.execute(stmt)
    rows = await cursor.fetchall()
    data = dict()
    for row in rows:
        data[row[0]] = {
            "name": row[1],
            "size": row[2],
            "client_username": row[5],
            "client_ram": row[8],
            "client_cpu": row[9],
        }

    await cursor.close()
    return json.dumps({"status": "OK", "data": data})


async def delete_client(payload: Dict[str, Any], db_conn: Connection) -> str:
    await db_conn.execute("UPDATE clients SET visible=FALSE WHERE id=?", (payload["id"], ) )
    await db_conn.commit()
    await close_connection(payload, db_conn)
    return json.dumps(
        {
            "status": "OK",
        }
    )


async def get_stats(payload: Dict[str, Any], db_conn: Connection) -> str:
    stmt = f"""
        SELECT count() as clients_count, sum(ram_size) as sum_of_ram, sum(cpu_count) as sum_of_cpu, sum(drive_size) as sum_of_drive
        FROM clients
        JOIN hard_drives
        ON clients.id=hard_drives.client_id;
            """

    cursor = await db_conn.execute(stmt)
    data = await cursor.fetchone()

    return json.dumps(
        {
            "status": "OK",
            "data": {
                "clients_count": data[0],
                "sum_of_ram": data[1],
                "sum_of_cpu": data[2],
                "sum_of_drive": data[3],
            },
        }
    )


async def update_client(payload: Dict[str, Any], db_conn: Connection) -> str:
    await db_conn.execute(
        "UPDATE clients SET ram_size=?, cpu_count=? WHERE id=?",
        (payload["ram_size"], payload["cpu_count"], payload["id"]),
    )
    await db_conn.execute(
        "UPDATE hard_drives SET drive_size=? WHERE client_id=?",
        (payload["drive_size"], payload["id"]),
    )
    await db_conn.commit()
    return json.dumps(
        {
            "status": "OK",
        }
    )
