import asyncio
import os
from signal import SIGTERM, SIGINT
from src.server import Server


async def main():
    settings_path = os.getenv("SETTINGS_PATH")
    if settings_path is None:
        raise RuntimeError("Settings not setted")

    app = await Server.from_config(settings_path)

    try:
        await app.listen_and_serve()
    except asyncio.CancelledError:
        await app.dispose()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    main_task = asyncio.ensure_future(main())
    for signal in [SIGINT, SIGTERM]:
        loop.add_signal_handler(signal, main_task.cancel)
    try:
        loop.run_until_complete(main_task)
    finally:
        loop.close()
