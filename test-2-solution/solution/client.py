from src.cli import Client

SERVER_HOST = "localhost"
SERVER_PORT = 8000


def main() -> None:
    client = Client(SERVER_HOST, SERVER_PORT)
    client.run()
        

if __name__ == "__main__":
    main()
