from datetime import datetime


# Спарведливо говоря, такое вынести бы в конфиги/файлы ресурсов, но не для такой задачи
SCHEDULE_LABEL = "Введите дату прибытия самолета по расписанию в формате HH:MM>"
FACT_LABEL = "Введите дату прибытия самолета по факту в формате: HH:MM>"
LATE_LABEL = "Самолет опаздывает"
BEFORE_LABEL = "Самолет прилетел раньше"
EQUAL_LABEL = "Самолет прилетел вовремя"


def str_to_time(string: str) -> datetime:
    return datetime.strptime(string, "%H:%M")


def check_is_late_or_not(schedule_arriving: datetime, fact_arriving: datetime) -> str:
    if schedule_arriving < fact_arriving:
        return LATE_LABEL
    if schedule_arriving > fact_arriving:
        return BEFORE_LABEL
    return EQUAL_LABEL


def main():
    arriving_by_schedule = input(SCHEDULE_LABEL)
    arriving_by_fact = input(FACT_LABEL)

    time_by_schedule = str_to_time(arriving_by_schedule)
    time_by_fact = str_to_time(arriving_by_fact)

    result = check_is_late_or_not(time_by_schedule, time_by_fact)
    print(result)


if __name__ == "__main__":
    main()
