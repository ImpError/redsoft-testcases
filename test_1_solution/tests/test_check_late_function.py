from datetime import datetime
from test_1_solution.main import (
    check_is_late_or_not,
    LATE_LABEL,
    BEFORE_LABEL,
    EQUAL_LABEL,
)


def test_plane_late():
    result = check_is_late_or_not(
        schedule_arriving=datetime.strptime("10:30", "%H:%M"),
        fact_arriving=datetime.strptime("11:30", "%H:%M"),
    )
    assert result == LATE_LABEL


def test_plane_before():
    result = check_is_late_or_not(
        schedule_arriving=datetime.strptime("11:30", "%H:%M"),
        fact_arriving=datetime.strptime("10:30", "%H:%M"),
    )
    assert result == BEFORE_LABEL


def test_plane_equal():
    result = check_is_late_or_not(
        schedule_arriving=datetime.strptime("10:30", "%H:%M"),
        fact_arriving=datetime.strptime("10:30", "%H:%M"),
    )
    assert result == EQUAL_LABEL
